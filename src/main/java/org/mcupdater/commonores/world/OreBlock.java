package org.mcupdater.commonores.world;

import org.mcupdater.commonores.render.OreBlockRender;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class OreBlock extends Block {
	
	public OreBlock(int id) {
		super(id, Material.rock);
	}
	
	@Override
	public int getRenderType() {
		return OreBlockRender.INSTANCE.renderId;
	}
}
