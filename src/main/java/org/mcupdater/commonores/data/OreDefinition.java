package org.mcupdater.commonores.data;

public class OreDefinition {
	public String	name;

	public int		metalColor;
	public boolean	ironAlternative;
	public boolean	goldAlternative;

	public String[]	alloyRecipe;

	public WorldGenPrefs	generation;
	public class WorldGenPrefs {
		public boolean	enabled;

		public int		oreColor;

		public int		minHeight;
		public int		maxHeight;
		public int		veinsPerChunk;
		public int		blocksPerVein;

		public String[]	biomeWhitelist;
	}

	public boolean isOreEnabled() {
		return generation != null && generation.enabled;
	}

	public boolean isAlloy() {
		return alloyRecipe != null && alloyRecipe.length > 0;
	}
}
