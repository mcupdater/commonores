package org.mcupdater.commonores.data;

import java.util.Set;

import com.google.common.collect.Sets;

public enum OreManager {
	INSTANCE;
	
	private Set<OreDefinition>	defs;

	private OreManager() {
		defs = Sets.newHashSet();
	}

	public static Set<OreDefinition> getDefs() {
		return INSTANCE.defs;
	}

	public static void registerDefinition(OreDefinition def) {
		INSTANCE.defs.add(def);
	}

}
