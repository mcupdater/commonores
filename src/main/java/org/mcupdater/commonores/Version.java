package org.mcupdater.commonores;

public class Version {
	public static final String	MOD_ID		= "commonores";
	public static final String	MOD_NAME	= "CommonOres";
	public static final String	VERSION		= "0.1";
}
