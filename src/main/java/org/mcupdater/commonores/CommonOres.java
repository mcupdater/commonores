package org.mcupdater.commonores;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.logging.Logger;

import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.MinecraftForge;

import org.mcupdater.commonores.data.DefaultSettings;
import org.mcupdater.commonores.data.OreDefinition;
import org.mcupdater.commonores.data.OreManager;

import com.google.gson.Gson;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;

@Mod(modid = Version.MOD_ID, name = Version.MOD_NAME, version = Version.VERSION, acceptedMinecraftVersions = "[1.6,1.7],", dependencies = "")
@NetworkMod(clientSideRequired = true, serverSideRequired = true)
public class CommonOres {
	public static Configuration	config;
	public static Logger		log	= Logger.getLogger(Version.MOD_NAME);

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent evt) {
		final File mcConfigDir = evt.getModConfigurationDirectory();
		final File configDir = new File(mcConfigDir, Version.MOD_ID);
		if (!configDir.exists()) configDir.mkdir();

		final File configFile = new File(configDir, Version.MOD_ID + ".cfg");
		config = new Configuration(configFile);
		config.load();

		// copy any default ore definitions out of resources if they don't exist
		for (final String metal : DefaultSettings.METAL_LIST) {
			log.info("Preparing " + metal);
			final File metalDefault = new File(configDir, metal + ".json");
			if (!metalDefault.exists()) {
				InputStream is = getClass().getResourceAsStream(
						"/defs/" + metal + ".json");
				if (is == null) {
					log.warning("Unable to find resource for " + metal);
					continue;
				} else {
					log.info("Found resource for " + metal);
					try {
						metalDefault.createNewFile();
						final FileOutputStream fos = new FileOutputStream(metalDefault);
						byte[] bytes = new byte[1024];
						int read = 0;
						while (( read = is.read(bytes) ) != -1) {
							fos.write(bytes, 0, read);
						}
						fos.close();
					} catch (IOException ioe) {
						log.severe("Exception writing " + metalDefault);
						ioe.printStackTrace();
					}
				}
			}
		}

		// load any ore definitions
		Gson gson = new Gson();
		
		for (final File file : configDir.listFiles()) {
			if (file.isFile() && file.canRead()
					&& file.getName().endsWith(".json")) {
				
				final BufferedReader reader;
				try {
					reader = Files.newBufferedReader(file.toPath(),
							StandardCharsets.UTF_8);
				} catch (IOException e) {
					log.warning("Unable to read " + file);
					continue;
				}
				
				OreDefinition json = gson.fromJson(reader, OreDefinition.class);
				if (json == null) {
					log.warning("Unable to parse " + file + " as JSON");
				} else {
					// make sure we've got this metal enabled :)
					final String key = json.name.toLowerCase();
					if (config.get(Configuration.CATEGORY_GENERAL,
							"metal." + key + ".enabled", true).getBoolean(true)) {
						
						// warn if an alloy is spawning ore
						if (json.isOreEnabled() && json.isAlloy()) {
							log.warning("Definition for " + key	+ " provides an alloy with oregen!");
						}
						
						OreManager.registerDefinition(json);
						System.out.println(gson.toJson(json));
					}
				}
			}
		}

		MinecraftForge.EVENT_BUS.register(this);
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent evt) {
		// TODO: register everything that was loaded from config or requested
		// during pre-init
	}

	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent evt) {
		if (config.hasChanged()) {
			config.save();
		}
	}
}
